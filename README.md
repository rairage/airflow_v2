# Instruções

##Build da imagem

docker run -ti -p 8080:8080 -v yourdag.py:/opt/airflow/dags/yourdag.py --entrypoint=/bin/bash apache/airflow:2.0.0b3-python3.8 -c '(airflow db init && airflow users create --username admin --password admin --firstname Anonymous --lastname Admin --role Admin --email admin@example.org); airflow webserver & airflow scheduler'

## Inicializar o DB
docker-compose run webserver db init

docker-compose run webserver users create -r Admin -u admin -e admin@example.com -f admin -l user -p admin

## Run start up Airflow
docker-compose up

Links de acesso:
1. Airflow Web UI (Credentials: `admin:admin`): http://localhost:8080
1. Flower UI: http://localhost:5555
